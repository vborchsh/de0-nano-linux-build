# DE0-Nano-SoC Kit Linux

This is a page about Terasic's Intel Cyclone V SE 5CSEMA4U23C6N based DE0-Nano-SoC Kit/Atlas-SoC Kit.

## Requirements

You should use Ubuntu, Debian or Fedora, 64bit

Create a build folder:

```
mkdir ~/de0-build
cd ~/de0-build
```

## ARM Cross compiler

Download pre-built x64 cross compiler:

```
wget -c https://releases.linaro.org/components/toolchain/binaries/6.5-2018.12/arm-linux-gnueabihf/gcc-linaro-
6.5.0-2018.12-x86_64_arm-linux-gnueabihf.tar.xz

tar xf gcc-linaro-6.5.0-2018.12-x86_64_arm-linux-gnueabihf.tar.xz

export CC=`pwd`/gcc-linaro-6.5.0-2018.12-x86_64_arm-linux-gnueabihf/bin/arm-linux-gnueabihf-
```

Let's test it:

```
${CC}gcc --version
```

You must see:

```
arm-linux-gnueabihf-gcc (Linaro GCC 6.5-2018.12) 6.5.0
Copyright (C) 2017 Free Software Foundation, Inc.
This is free software; see the source for copying conditions. There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

```

## Build U-Boot

Clone 2019.07 release:

```
git clone -b v2019.07 https://github.com/u-boot/u-boot --depth=1

cd u-boot/
```

Download patch for this board:

```
wget -c https://github.com/eewiki/u-boot-patches/raw/master/v2019.07/0001-de0_nano-fixes.patch

patch -p1 < 0001-de0_nano-fixes.patch
```

Configure and build:

```
make ARCH=arm CROSS_COMPILE=${CC} distclean

make ARCH=arm CROSS_COMPILE=${CC} socfpga_de0_nano_soc_defconfig

make ARCH=arm CROSS_COMPILE=${CC} u-boot-with-spl.sfp
```

## Linux Kernel

Download Kernel repository and checkout to required version:

```
git clone https://github.com/RobertCNelson/socfpga-kernel-dev

cd socfpga-kernel-dev/

git checkout origin/v4.14.x -b tmp
```

Let's build it:

```
./build_kernel.sh
```

## Root file system

Here we will use an Ubuntu 20.04 LTS as root files system

Download and verify:

```
wget -c https://rcn-ee.com/rootfs/eewiki/minfs/ubuntu-20.04-minimal-armhf-2020-05-10.tar.xz

sha256sum ubuntu-20.04-minimal-armhf-2020-05-10.tar.xz
```

You must see:

```
de0177ac9259fdbcc626ee239f4258b64070c0921dbc38c45fab6925a5becaa1  ubuntu-20.04-minimal-armhf-2020-05-10.tar.xz
```

Extract:

```
tar xf ubuntu-20.04-minimal-armhf-2020-05-10.tar.xz
```

## Setup microSD card

Connect SD card to your PC and find her device path:

```
lsblk
```

Let's say it's /dev/sdb, for exmaple. Then export DISC variable:

```
export DISK=/dev/sdb
```

Then erase card by zeros:

```
sudo dd if=/dev/zero of=${DISK} bs=1M count=64
```

Check sfdisk utilites:

```
sudo sfdisk --version
sfdisk from util-linux 2.27.1
```

For sfdisk >= 2.26.x:

```
sudo sfdisk ${DISK} <<-__EOF__
1M,1M,0xA2,
2M,,,*
__EOF__
```

For sfdisk <= 2.25.x:

```
sudo sfdisk --unit M ${DISK} <<-__EOF__
1,1,0xA2,
2,,,*
__EOF__
```

Install Bootloader to sd card:

```
sudo dd if=./u-boot/u-boot-with-spl.sfp of=${DISK}1
```

Format partition for rootfs:

````
sudo mkfs.ext4 -L rootfs ${DISK}2
````

Mount partition:

```
sudo mkdir -p /media/rootfs/

sudo mount ${DISK}2 /media/rootfs/
```

## Install Kernel and Root Files System

Exactly Kernel version you saw when you build Kernel. The kernel building scripts listed on this page will now give you a hint of what
kernel version was built.

```
-----------------------------
Script Complete
eewiki.net: [user@localhost:~$ export kernel_version=5.X.Y-Z]
-----------------------------
```

Just export last string in [...]:

```
export kernel_version=5.X.Y-Z
```

### Copy rootfs:

```
sudo tar xfvp ./*-*-*-armhf-*/armhf-rootfs-*.tar -C /media/rootfs/
sync
sudo chown root:root /media/rootfs/
sudo chmod 755 /media/rootfs/
```

### Setup extlinux.conf

```
sudo mkdir -p /media/rootfs/boot/extlinux/
sudo sh -c "echo 'label Linux ${kernel_version}' > /media/rootfs/boot/extlinux/extlinux.conf"
sudo sh -c "echo '
kernel /boot/vmlinuz-${kernel_version}' >> /media/rootfs/boot/extlinux/extlinux.conf"
sudo sh -c "echo '
append root=/dev/mmcblk0p2 ro rootfstype=ext4 rootwait quiet' >> /media/rootfs/boot
/extlinux/extlinux.conf"
sudo sh -c "echo '
fdtdir /boot/dtbs/${kernel_version}/' >> /media/rootfs/boot/extlinux/extlinux.conf"
```

### Copy Kernel Image:

```
sudo cp -v ./socfpga-kernel-dev/deploy/${kernel_version}.zImage /media/rootfs/boot/vmlinuz-${kernel_version}
```

### Copy Kernel device tree:

```
sudo mkdir -p /media/rootfs/boot/dtbs/${kernel_version}/
sudo tar xfv ./socfpga-kernel-dev/deploy/${kernel_version}-dtbs.tar.gz -C /media/rootfs/boot/dtbs
/${kernel_version}/
```

### Copy kernel modules:

```
sudo tar xfv ./socfpga-kernel-dev/deploy/${kernel_version}-modules.tar.gz -C /media/rootfs/
```

### Files system table (/etc/fstab):

```
sudo sh -c "echo '/dev/mmcblk0p2 / auto errors=remount-ro 0 1' >> /media/rootfs/etc/fstab"
```

### Remove SD card:

```
sync
sudo umount /media/rootfs
```

Insert in to board and run it!